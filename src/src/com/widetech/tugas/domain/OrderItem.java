package com.widetech.tugas.domain;

public class OrderItem {
	private String code;
	private String name;
	private String type;
	private double price;
	private int quantity;
	
	public OrderItem (String code, String name, String type, double price, int quantity) {
		this.code = code;
		this.name = name;
		this.type = type;
		this.price = price;
		this.quantity = quantity;
	}
	public String getCode() {
		return code;
	}
	
	public String getName() {
		return name;
	}
	
	public String getType() {
		return type;
	}
	
	public double getPrice() {
		return price;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double totalPrice() {
		return this.price * this.quantity;
	}
	
	
}
