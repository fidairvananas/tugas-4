package com.widetech.tugas.service;

public class BusinessServiceException extends Exception{
	
	private static final long serialVersionUID = -4843052918855423806L;

	public BusinessServiceException() {
		super();
	}
	
	public BusinessServiceException(String message) {
		super(message);
	}
	
}
