package com.widetech.tugas.service;

import java.sql.Connection;

import com.widetech.tugas.domain.Order;
import com.widetech.tugas.presistance.DatabaseConnection;
import com.widetech.tugas.presistance.OrderRepository;
import com.widetech.tugas.presistance.RepositoryException;
import com.widetech.tugas.presistance.impl.OrderRepositoryMysql;

public class OrderService {
	public void saveOrderWithOrderItem(Order order) throws BusinessServiceException {
		Connection con = null;
		try {
			con = DatabaseConnection.getPoolConnection();
			DatabaseConnection.beginTransaction(con);
			
			OrderRepository orderRepo = new OrderRepositoryMysql(con);
			
			orderRepo.save(order);
			
			DatabaseConnection.commitTransaction(con);
			DatabaseConnection.closeTransaction(con);
		} catch (RepositoryException e) {
			if (con != null) {
				try {
					DatabaseConnection.rollBackTransaction(con);			
				} catch (Exception e2) {
					throw new BusinessServiceException("Service error !");
				}
			}
		} 
	}
}
