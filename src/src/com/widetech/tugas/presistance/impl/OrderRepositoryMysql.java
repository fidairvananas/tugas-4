package com.widetech.tugas.presistance.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.widetech.tugas.domain.Order;
import com.widetech.tugas.domain.OrderItem;
import com.widetech.tugas.presistance.DatabaseConnection;
import com.widetech.tugas.presistance.OrderRepository;
import com.widetech.tugas.presistance.RepositoryException;
import com.widetech.tugas.service.BusinessServiceException;


public class OrderRepositoryMysql implements OrderRepository{
	private Connection con;
	
	public OrderRepositoryMysql(Connection con) {
		this.con = con;
	}
	
	public List<Order> findAll() throws RepositoryException {
		
		try {
			
			con = DatabaseConnection.getPoolConnection();
			
			String sqlString = "SELECT * FROM order_tbl o JOIN orderItem_tbl ON o.id = OrderId ";
			
			List<Order> listOrder = new ArrayList<Order>();
			HashSet<Integer> idOrder = new HashSet<Integer>();
			
			PreparedStatement stm = con.prepareStatement(sqlString);
			ResultSet res = stm.executeQuery();
			
			
			while (res.next()) {
			    
			    OrderItem item = new OrderItem(res.getString("code"), res.getString("name"),res.getString("type") , res.getDouble("price"), res.getInt("qty"));
			    if (idOrder.contains(res.getInt("id"))) {
			    	for (Order order : listOrder) {
						if (res.getInt("id") == order.getId()) {
							order.addItem(item);		
						} 
					}
				} else {
					Order tempOrder = new Order(res.getInt("id"));
					tempOrder.addItem(item);
					listOrder.add(tempOrder);
					idOrder.add(res.getInt("id"));
				}
			    
			}
			
			con.close();
			
			return listOrder;
			
			
		} catch (SQLException | BusinessServiceException e) {
			throw new RepositoryException("Find all order failed !");
		} 
	}
	
	@Override
	public Order findById(int orderId) throws RepositoryException {
		
		try {
			
			con = DatabaseConnection.getPoolConnection();
			
			String sqlString = "SELECT * FROM order_tbl o JOIN orderItem_tbl ON o.id = OrderId WHERE o.id ="+ orderId;
			
			Statement stm = con.createStatement();
			ResultSet res = stm.executeQuery(sqlString);
			
			Order order = new Order(orderId);
			
			while (res.next()) {
				OrderItem items = new OrderItem(res.getString("code"), res.getString("name"),res.getString("type") , res.getDouble("price"), res.getInt("qty"));
				if (res.getInt("OrderId") == orderId) {
					order.addItem(items);
				} 

			}
			
			con.close();
			return order;
			
			
		} catch (SQLException | BusinessServiceException e) {
			throw new RepositoryException("Find order by id failed !");
		} 
	}


	@Override
	public void save(Order order) throws RepositoryException {
			
		try {
			String sqlOrder = "INSERT INTO order_tbl(createdAt, note) VALUES (?,?)";
			
			PreparedStatement stmOrder;
			stmOrder = con.prepareStatement(sqlOrder, Statement.RETURN_GENERATED_KEYS);
			stmOrder.setDate(1, new java.sql.Date(order.getCreatedAt().getTime()));
			stmOrder.setString(2, order.getNote());
			
			stmOrder.executeUpdate();
			ResultSet rs = stmOrder.getGeneratedKeys();
			
			
			String sqlItem = "INSERT INTO orderitem_tbl(code, name, type, price, qty, OrderId) VALUES (?,?,?,?,?,?)";
			
			PreparedStatement stmItem = con.prepareStatement(sqlItem);
			
			while (rs.next()) {
				for (OrderItem item : order.getOrderItem()) {
					if (item != null) {
						stmItem.setString(1, item.getCode());
						stmItem.setString(2, item.getName());
						stmItem.setString(3, item.getType());
						stmItem.setDouble(4, item.getPrice());
						stmItem.setInt(5, item.getQuantity());
						stmItem.setInt(6, rs.getInt(1));	
					}
				}
				
			}
			
			stmItem.executeUpdate();
			
		} catch (SQLException e) {
			throw new RepositoryException("Save order failed !");
		}
		
	}
	
}
