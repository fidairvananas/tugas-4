package com.widetech.tugas.presistance;

import java.io.IOException;
import java.util.List;

import com.widetech.tugas.domain.Order;

public interface OrderRepository {
	
	List<Order> findAll() throws RepositoryException, IOException;
	
	Order findById(int id) throws RepositoryException;
	
	void save(Order order) throws RepositoryException;
}
