package com.widetech.tugas.domain;

import java.util.Date;

public class Order {
	private int id;
	private Date createdAt;
	private String note;
	

	private OrderItem[] orderItem = new OrderItem[5];
	
	
	public Order(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}



	
	public Date getCreatedAt() {
		return createdAt;
	}
	
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getNote() {
		return note;
	}
	
	public void setNote(String note) {
		this.note = note;
	}
	
	public double totalPrice() {
		double total = 0;
		for(OrderItem item : orderItem) {
			if (item != null) {
				total = total + item.totalPrice();
			}
		}
		return total;
	}

	public void addItem(OrderItem orderItem) {
		for(int i = 0; i < this.orderItem.length; i++) {
			if(this.orderItem[i] == null) {
				this.orderItem[i] = orderItem;
				break;
			}
		}
	}

	public OrderItem[] getOrderItem() {
		return orderItem;
	}
	
	public void setOrderItem(OrderItem[] orderItem) {
		this.orderItem = orderItem;
	}
}
