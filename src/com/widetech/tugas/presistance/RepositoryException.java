package com.widetech.tugas.presistance;

public class RepositoryException extends Exception{

	private static final long serialVersionUID = -9085037606466543947L;
	
	public RepositoryException() {
		super();
	}
	
	public RepositoryException(String message) {
		super(message);
	}
	
}
