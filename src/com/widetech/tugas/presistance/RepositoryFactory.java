package com.widetech.tugas.presistance;


import com.widetech.tugas.presistance.impl.OrderRepositoryFile;
import com.widetech.tugas.presistance.impl.OrderRepositoryMysql;

public class RepositoryFactory {
	public static String dbType = "Mysql";
	
	public static OrderRepository getOrderRepository() {
		if (dbType == "File") {
			return new OrderRepositoryFile();
		} else {
			return new OrderRepositoryMysql(null);
		}
	}
}
