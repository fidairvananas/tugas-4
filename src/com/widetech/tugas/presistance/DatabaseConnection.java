package com.widetech.tugas.presistance;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.widetech.tugas.service.BusinessServiceException;

public class DatabaseConnection {
	protected static String userDb = "root";
	protected static String pass = "";
	protected static String jdbcUrl = "jdbc:mysql://localhost/order_db";
	
	public static Connection getPoolConnection() throws BusinessServiceException {
		Connection con = null;
		try {
			ComboPooledDataSource ds = new ComboPooledDataSource();
			ds.setDriverClass("com.mysql.jdbc.Driver");
			ds.setJdbcUrl(jdbcUrl);
			ds.setUser(userDb);
			ds.setPassword(pass);
			ds.setMinPoolSize(3);
			
			con = ds.getConnection();
			
		}  catch (SQLException | PropertyVetoException e) {
			throw new BusinessServiceException("Server not connected!");
		}
		return con;
	}
	
	public static void beginTransaction(Connection con) throws BusinessServiceException {
		try {
			con.setAutoCommit(false);
		} catch (SQLException e) {
			throw new BusinessServiceException("Failed to begin transaction");
		}
	}
	
	public static void commitTransaction(Connection con) throws BusinessServiceException {
		try {
			con.commit();
		} catch (SQLException e) {
			throw new BusinessServiceException("Failed to commit transaction");
		}
	}
	
	public static void closeTransaction(Connection con) throws BusinessServiceException {
		try {
			con.close();
		} catch (SQLException e) {
			throw new BusinessServiceException("Failed to close transaction");
		}
	}
	
	public static void rollBackTransaction(Connection con) throws BusinessServiceException {
		try {
			con.rollback();
		} catch (SQLException e) {
			throw new BusinessServiceException("Failed to roolback transaction");
		}
	}
}
