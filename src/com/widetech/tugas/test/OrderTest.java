package com.widetech.tugas.test;

import java.io.IOException;
import java.util.List;

import com.widetech.tugas.domain.Order;
import com.widetech.tugas.domain.OrderItem;
import com.widetech.tugas.presistance.OrderRepository;
import com.widetech.tugas.presistance.RepositoryException;
import com.widetech.tugas.presistance.RepositoryFactory;

public class OrderTest {

	public static void main(String[] args) {
		
		OrderRepository orderRepo = RepositoryFactory.getOrderRepository();
		
		List<Order> listOrder;
		
		try {
			
			listOrder = orderRepo.findAll();
			
			for (Order order : listOrder) {
				int numb = 1;
				System.out.println("Order Id : " + order.getId());
				
				for (OrderItem item : order.getOrderItem()) {
					if (item != null) {
						System.out.format("     " +numb +". Code: "+ item.getCode() + ", Name: "+ item.getName() + ", Type: " + item.getType() + ", Price: "+ item.getPrice() + ", Qty: " + item.getQuantity());
						System.out.println(", Total Price: " + item.totalPrice());
						numb++;
					}
				}
				
				System.out.println("Total Price: " + order.totalPrice());
				System.out.println(" ");
			}
		} catch (RepositoryException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} 
		
		
		

	}
	

}
