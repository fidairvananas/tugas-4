package com.widetech.tugas.test;

import java.util.Date;

import com.widetech.tugas.domain.Order;
import com.widetech.tugas.domain.OrderItem;
import com.widetech.tugas.service.BusinessServiceException;
import com.widetech.tugas.service.OrderService;

public class OrderSaveTest {

	public static void main(String[] args) {
		try {
			OrderItem item = new OrderItem("214", "Orange", "Fruit", 11000, 1);
			Order order = new Order(0);
			order.setNote("Test");
			order.setCreatedAt(new Date());
			order.addItem(item);
			
			OrderService service = new OrderService();
			service.saveOrderWithOrderItem(order);
			System.out.println("Order saved!");
		} catch (BusinessServiceException e) {
			System.out.println("Failed save order!");
		}
	}

}
