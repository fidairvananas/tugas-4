package com.widetech.tugas.test;


import com.widetech.tugas.domain.Order;
import com.widetech.tugas.domain.OrderItem;
import com.widetech.tugas.presistance.OrderRepository;
import com.widetech.tugas.presistance.RepositoryFactory;

public class FindByIdTest {

	public static void main(String[] args) {
		try {
			OrderRepository orderRepo = RepositoryFactory.getOrderRepository();
			
			Order orderById = orderRepo.findById(1);
			
			System.out.println("Order ID: " + orderById.getId());
			int numb = 1;
			for (OrderItem item : orderById.getOrderItem()) {
				if (item != null) {
					System.out.println("     " +numb +". Code: "+ item.getCode() + ", Name: "+ item.getName() + ", Type: " + item.getType() + ", Price: "+ item.getPrice() + ", Qty: " + item.getQuantity());
					System.out.println("        " + "Total Price: " + item.totalPrice());
					numb++;					
				}
			}
			System.out.println("\n Total Price: " + orderById.totalPrice());
			
		} catch (Exception e) {
			System.out.println(e.getMessage());;
		}

	}

}
