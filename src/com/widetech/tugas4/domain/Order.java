package com.widetech.tugas4.domain;

import java.util.Date;

public class Order {
	private int id;
	private Date createdAt = new Date();
	private OrderItem[] orderItem = new OrderItem[5];
	
	
	public Order(Date createdAt, int id) {
		this.createdAt = createdAt;
		this.id = id;
	}
	
	public double totalPrice() {
		double total = 0;
		for(OrderItem item : orderItem) {
			if (item != null) {
				total = total + item.totalPrice();
			}
		}
		return total;
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}

	public int getId() {
		return id;
	}

	public void addItem(OrderItem orderItem) {
		for(int i = 0; i < this.orderItem.length; i++) {
			if(this.orderItem[i] == null) {
				this.orderItem[i] = orderItem;
				break;
			}
		}
	}

	public OrderItem[] getOrderItem() {
		return orderItem;
	}
}
