package com.widetech.tugas4.presistance;

import com.widetech.tugas4.presistance.impl.OrderRepositoryFile;
import com.widetech.tugas4.presistance.impl.OrderRepositoryMysql;

public class RepositoryFactory {
	public static String dbType = "Mysql";
	
	public static OrderRepository getOrderRepository() {
		if (dbType == "File") {
			return new OrderRepositoryFile();
		} else {
			return new OrderRepositoryMysql();
		}
	}
}
