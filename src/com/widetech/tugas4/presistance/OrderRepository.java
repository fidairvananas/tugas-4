package com.widetech.tugas4.presistance;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.widetech.tugas4.domain.Order;

public interface OrderRepository {
	
	List<Order> findAll() throws IOException, SQLException;
	
	Order findById(int id);
	
	void save(Order order);
}
