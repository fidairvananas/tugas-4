package com.widetech.tugas4.presistance.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.widetech.tugas4.domain.Order;
import com.widetech.tugas4.domain.OrderItem;
import com.widetech.tugas4.presistance.OrderRepository;

public class OrderRepositoryFile implements OrderRepository {

	private List<Order> listOrder;

	public List<Order> findAll() throws IOException{
		File file = new File("order.txt");
		try {
			
			FileReader fileReader = new FileReader(file);
			BufferedReader bufReader = new BufferedReader(fileReader);
			
			int id;
			String code;
			String name;
			String type;
			double price;
			int quantity;
			
			listOrder = new ArrayList<Order>();
			String line = null;
			while ((line = bufReader.readLine()) != null) {
				String[] data = line.split(";");
				
				id = Integer.parseInt(data[0]);
				code = data[1];
				name = data[2];
				type = data[3];
				price = Double.parseDouble(data[4].trim());
				quantity = Integer.parseInt(data[5].trim());
				
				OrderItem item = new OrderItem(code, name, type, price, quantity);
				
				if (findById(id) == null) {
					Order orderTemp = new Order(new Date(), id);
					orderTemp.addItem(item);
					listOrder.add(orderTemp);	
				} else {
					int idx = listOrder.indexOf(findById(id));
					listOrder.get(idx).addItem(item);
				}
				
			}
			
			return listOrder;
		} catch (FileNotFoundException e) {
			throw new FileNotFoundException("File not found !");
		} catch (IOException e) {
			throw new IOException("IO Problem !");
		}
	}
	
	
	public Order findById(int findId) {
	    for ( Order orderItem : listOrder) {
			if (orderItem.getId() == findId) {
				return orderItem;
			}
		}
	    return null;
	}


	@Override
	public void save(Order order) {
		return;
	}
	
	
	
}
