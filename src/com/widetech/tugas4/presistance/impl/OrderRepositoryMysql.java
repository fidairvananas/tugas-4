package com.widetech.tugas4.presistance.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.widetech.tugas4.domain.Order;
import com.widetech.tugas4.domain.OrderItem;
import com.widetech.tugas4.presistance.OrderRepository;


public class OrderRepositoryMysql implements OrderRepository{
	
	private List<Order> listOrder;
	
	public List<Order> findAll() throws SQLException{
		final String userDb = "root";
		final String pass = "";
		final String jdbcUrl = "jdbc:mysql://localhost/order_db";
		
		try {
			
			Connection con = DriverManager.getConnection(jdbcUrl, userDb, pass);
			String sqlString = "SELECT * FROM order_tbl o JOIN orderItem_tbl ON o.id = OrderId ";
			
			LinkedList<Order> listOrder = new LinkedList<Order>();
			HashSet<Integer> idOrder = new HashSet<Integer>();
			
			Statement stm = con.createStatement();
			ResultSet res = stm.executeQuery(sqlString);
			
			
			while (res.next()) {
			    
			    OrderItem item = new OrderItem(res.getString("code"), res.getString("name"),res.getString("type") , res.getDouble("price"), res.getInt("qty"));
			    if (idOrder.contains(res.getInt("id"))) {
			    	for (Order order : listOrder) {
						if (res.getInt("id") == order.getId()) {
							order.addItem(item);		
						} 
					}
				} else {
					Order tempOrder = new Order(new Date(), res.getInt("id"));
					tempOrder.addItem(item);
					listOrder.add(tempOrder);
					idOrder.add(res.getInt("id"));
				}
			    
			}
			
			con.close();
			
			return listOrder;
			
			
		} catch (SQLException e) {
			throw new SQLException("Database connection failed !");
		}
	}
	
	public Order findById(int orderId) {
		for (Order listOrder : listOrder) {
			if(listOrder != null) {
				return listOrder;
			}
		}
		return null;
	}


	@Override
	public void save(Order order) {
		return;
	}
	
}
