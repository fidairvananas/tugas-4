package com.widetech.tugas4.test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.widetech.tugas4.domain.Order;
import com.widetech.tugas4.domain.OrderItem;
import com.widetech.tugas4.presistance.OrderRepository;
import com.widetech.tugas4.presistance.RepositoryFactory;

public class OrderTest {

	public static void main(String[] args) {
		
		OrderRepository orderRepo = RepositoryFactory.getOrderRepository();
		
		List<Order> listOrder;
		try {
			listOrder = orderRepo.findAll();
			for (Order order : listOrder) {
				int numb = 1;
				System.out.println("Order Id : " + order.getId());
				
				for (OrderItem item : order.getOrderItem()) {
					if (item != null) {
						System.out.format("     " +numb +". Code: "+ item.getCode() + ", Name: "+ item.getName() + ", Type: " + item.getType() + ", Price: "+ item.getPrice() + ", Qty: " + item.getQuantity());
						System.out.println("Total Price: " + item.totalPrice());
						numb++;
					} else {
						continue;
					}
				}
				
				System.out.println("Total Price: " + order.totalPrice());
				System.out.println(" ");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		

	}
	

}
